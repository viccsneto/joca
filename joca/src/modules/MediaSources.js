import rainsfx from '../sfx/rain.ogg';
import rainimg from '../img/rain.png';
import riversfx from '../sfx/river.ogg';
import riverimg from '../img/river.png';
import beethovensfx from '../sfx/beethoven.ogg';
import musicalnotesimg from '../img/musicalnotes.png';
import fansfx from '../sfx/fan.ogg';
import fanimg from '../img/fan.png';
import windsfx from '../sfx/wind.ogg';
import windimg from '../img/wind.png';
import stormsfx from '../sfx/storm.ogg';
import stormimg from '../img/storm.png';
import firesfx from '../sfx/fire.ogg';
import fireimg from '../img/fire.png';
import brownnoisesfx from '../sfx/brownnoise.ogg';
import brownnoiseimg from '../img/brownnoise.png';
import pinknoisesfx from '../sfx/pinknoise.ogg';
import pinknoiseimg from '../img/pinknoise.png';
import whitenoisesfx from '../sfx/whitenoise.ogg';
import whitenoiseimg from '../img/whitenoise.png';

const MediaSources = {
  rain: {
    audio: rainsfx,
    image: rainimg
  },
  river: {
    audio: riversfx,
    image: riverimg
  },
  beethoven: {
    audio: beethovensfx,
    image: musicalnotesimg
  },
  wind: {
    audio: windsfx,
    image: windimg
  },
  fire: {
    audio: firesfx,
    image: fireimg
  },
  storm: {
    audio: stormsfx,
    image: stormimg
  },
  fan: {
    audio: fansfx,
    image: fanimg
  },
  brownnoise: {
    audio: brownnoisesfx,
    image: brownnoiseimg
  },
  pinknoise: {
    audio: pinknoisesfx,
    image: pinknoiseimg
  },
  whitenoise: {
    audio: whitenoisesfx,
    image: whitenoiseimg
  }
};

export default MediaSources;
