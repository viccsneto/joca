import React from "react";
import ReactHowler from 'react-howler';
import { Card, H3, Slider } from "@blueprintjs/core";

const VOLUME_FACTOR = 10.0;
//import { Example, handleBooleanChange, IExampleProps } from "@blueprintjs/docs-theme";


class SoundPanel extends React.Component {
  changeVolume(vol)
  {
    let newState = this.state;
    newState.volume = vol / VOLUME_FACTOR;
    this.setState(newState);
  }

  constructor(props) {
    super(props);
    this.state = {
      mediasource: this.props.mediasource,
      volume: this.props.volume !== null? this.props.volume: VOLUME_FACTOR * 0.5,
      source: this.props.source || "rain",
    }

  }

  

  componentDidMount()
  {    
    this.changeVolume.bind(this);    
  }

  render()
  {
    console.log(this.state.source);
    const selectedAudio = this.state.mediasource[this.state.source].audio;
    const selectedPicture = this.state.mediasource[this.state.source].image;
    let component = <ReactHowler src={selectedAudio} volume={this.state.volume} loop={true} playing={true} preload={true}/>;

    if (this.state.volume === 0) {
      component = <ReactHowler src={selectedAudio} volume={this.state.volume} loop={true} playing={false} preload={true} seek={0}/>;
    }

    return (      
      <Card className="soundpanel">
        <div  className='pt-elevation pt-card'>
          <H3>{this.state.source}</H3>        
          <img src={selectedPicture} className="SoundImage" alt={this.state.source + " picture"}/>                    
          <div  className="VolumeSlider">
            <Slider
              min={0}
              max={10}
              stepSize={1}
              labelStepSize={1}
              onChange={(val) => this.changeVolume(val)}            
              value={this.state.volume * VOLUME_FACTOR}        
            />
          </div>
          {component}
        </div>              
      </Card>
    )
  }
}

export default SoundPanel;