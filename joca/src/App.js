import React, { Component } from 'react';

import MediaSources from './modules/MediaSources';
import SoundPanel from './modules/controls/SoundPanel';
import './App.css';
import '../node_modules/@blueprintjs/core/lib/css/blueprint.css';
import '../node_modules/@blueprintjs/icons/lib/css/blueprint-icons.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="SoundRack">
          <SoundPanel mediasource={MediaSources} source="storm" volume={0.0} />
          <SoundPanel mediasource={MediaSources} source="rain" volume={0.0} />
          <SoundPanel mediasource={MediaSources} source="river" volume={0.0} />
        </div>
        <div className="SoundRack">
          <SoundPanel mediasource={MediaSources} source="beethoven" volume={0.0} />
          <SoundPanel mediasource={MediaSources} source="wind" volume={0.0} />
          <SoundPanel mediasource={MediaSources} source="fan" volume={0.0} />
        </div>
        <div className="SoundRack">
          <SoundPanel mediasource={MediaSources} source="brownnoise" volume={0.0} />
          <SoundPanel mediasource={MediaSources} source="pinknoise" volume={0.0} />
          <SoundPanel mediasource={MediaSources} source="whitenoise" volume={0.0} />
        </div>
        <div className="SoundRack">
          <SoundPanel mediasource={MediaSources} source="fire" volume={0.0} />
          <SoundPanel mediasource={MediaSources} source="pinknoise" volume={0.0} />
          <SoundPanel mediasource={MediaSources} source="whitenoise" volume={0.0} />
        </div>
      </div>
    );
  }
}

export default App;
